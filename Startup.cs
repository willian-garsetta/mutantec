﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace ProjetoMutante
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) =>
            {
                //await context.Response.WriteAsync("Hello World!");
                //var a = Task<string>.Factory.StartNew(() =>  new controller.UserController())
                var jsonContent = new controller.UserController().getUsers().Result;
                var ListUsers = JsonConvert.DeserializeObject<List<model.User>>(jsonContent);
                
                string strResult= "<p>1) Web Sites <br />";
                foreach (model.User Users in ListUsers)
                {
                    strResult += Users.website + "<br />";
                }
                strResult += "</p>";
                await context.Response.WriteAsync(strResult);

                strResult= "<p>2) Nome, email,Empresa<br />";
                var ListUsersOrder = ListUsers.OrderBy(ordName => ordName.name);
                foreach (model.User Users in ListUsersOrder)
                {
                    strResult += Users.name + " | " + Users.email + " | " + Users.company.name + "<br />";
                }
                strResult += "</p>";
                await context.Response.WriteAsync(strResult);

                strResult= "<p>3) Usuarios com a palavra 'Suite' no endereco: <br />";
                foreach (model.User Users in ListUsersOrder)
                {
                    string strAddress = Users.address.suite;
                    if (Users.address.suite.ToUpper().Contains("SUITE"))
                    {
                        strResult +=Users.name + "<br />";
                    }
                }
                strResult += "</p>";
                await context.Response.WriteAsync(strResult);
            });
        }
    }
}
