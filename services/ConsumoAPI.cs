using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoMutante.service
{
    class ConsumoApi
    {
        HttpClient client;

        public async Task<string> getContact()
        {
            string ret  = null;
             var endPoint = await client.GetAsync("/users");

                 ret = await endPoint.Content.ReadAsStringAsync();

            

            return ret;
        }
    
        public ConsumoApi()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("https://jsonplaceholder.typicode.com");

        }

    }
}
